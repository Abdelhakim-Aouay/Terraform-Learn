provider "aws" {
  region = var.region
}
variable "region" {}     # variable environement
variable "avail_zone" {} # variable environement
variable "vpc_cidr_block" {}
variable "subnet_cidr_block" {}
variable "env_prefix" {}
variable "my_ip" {}
variable "instance_type" {}
variable "public_key_location" {}

# create VPC and Subnet

resource "aws_vpc" "myapp-vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
    Name = "${var.env_prefix}-vpc"
  }
}

resource "aws_subnet" "myapp-subnet-1" {
  vpc_id            = aws_vpc.myapp-vpc.id
  cidr_block        = var.subnet_cidr_block
  availability_zone = var.avail_zone
  tags = {
    Name = "${var.env_prefix}-subnet-1"
  }
}

# create internet gateway et route table
resource "aws_internet_gateway" "my-app-gateway" {
  vpc_id = aws_vpc.myapp-vpc.id

  tags = {
    Name = "${var.env_prefix}-igw"
  }
}
# si on veut creer notre route table et on l'associate a un subnet on fait les deux etapes en bas
/*
resource "aws_route_table" "myapp-route-table" {
  vpc_id = aws_vpc.myapp-vpc.id

  route { 
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my-app-gateway.id
  }
  tags = {
    Name = "${var.env_prefix}-rtb"
  }
}

resource "aws_route_table_association" "a-rtb-subnet" {
  subnet_id      = aws_subnet.myapp-subnet-1.id
  route_table_id = aws_route_table.myapp-route-table.id
}*/

# si on veur utuliser le route table par defaut(se creer automatiquement lorsue on cree un vpc) on travaille comme ci dessous:

resource "aws_default_route_table" "myapp-route-table-default" {
  default_route_table_id = aws_vpc.myapp-vpc.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my-app-gateway.id
  }
  tags = {
    Name = "${var.env_prefix}-main-rtb"
  }
}
# creation de security groups
/*resource "aws_security_group" "myapp-sg" {
  name        = "myapp-sg"
  description = "Allow ssh inbound traffic"
  vpc_id      = aws_vpc.myapp-vpc.id*/
resource "aws_default_security_group" "default-myapp-sg" {

  vpc_id = aws_vpc.myapp-vpc.id

  ingress {
    description = "ssh from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.my_ip]

  }

  ingress {
    description = "internet from VPC"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]

  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]

  }

  tags = {
    Name = "${var.env_prefix}-default-sg"
  }
}

data "aws_ami" "latest-amazon-linux-image" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

/*resource "aws_key_pair" "ssh-key" {
  key_name   = "server-key"
  public_key = "${file(var.public_key_location)}"
}*/

resource "aws_instance" "myapp-ec2" {
  ami                         = data.aws_ami.latest-amazon-linux-image.id
  instance_type               = var.instance_type
  subnet_id                   = aws_subnet.myapp-subnet-1.id
  vpc_security_group_ids      = [aws_default_security_group.default-myapp-sg.id]
  availability_zone           = var.avail_zone
  associate_public_ip_address = true
  #key_name = aws_key_pair.ssh-key.key_name
  key_name = "terraform-key"
  #user_data = file("script.sh")

  tags = {
    Name = "${var.env_prefix}-ec2"
  }

  connection {
    type        = "ssh"
    user        = "ec2-user"
    private_key = file(var.public_key_location) # Remplacez par le chemin de votre clé privée
    host        = self.public_ip
  }

  provisioner "file" {
    source      = "script.sh"
    destination = "/home/ec2-user/script-server.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /home/ec2-user/script-server.sh",
      "sudo /home/ec2-user/script-server.sh"
    ]
  }

  provisioner "local-exec" {
    command = "echo ${self.public_ip} > oupout.txt"
    
  }
}

output "test-ami" {
  value = data.aws_ami.latest-amazon-linux-image.id

}
output "pubic_ip" {
  value = aws_instance.myapp-ec2.public_ip

}